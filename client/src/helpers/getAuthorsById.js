function getAuthorsById(authorsList, authorsIdList) {
	return authorsList.reduce((acc, cur) => {
		authorsIdList.includes(cur.id) && acc.push(cur);
		return acc;
	}, []);
}

export default getAuthorsById;
