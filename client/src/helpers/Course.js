class Course {
	constructor({ title, description, duration, authors }) {
		this.title = title;
		this.description = description;
		this.duration = +duration;
		this.authors = authors;
	}
}

export default Course;
