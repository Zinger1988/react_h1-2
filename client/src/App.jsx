import React, { useState } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';

import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import Header from './components/Header/Header';
import CourseContainer from './components/CourseContainer/CourseContainer';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import Page404 from './components/Page404/Page404';

import './App.scss';

function App() {
	const [user, setUser] = useState(
		(localStorage.getItem('user') &&
			JSON.parse(localStorage.getItem('user'))) ||
			null
	);
	const isAllowed = localStorage.getItem('token');

	return (
		<div className='app'>
			<Header className='app__header' user={user} setUser={setUser} />
			<main className='app__content'>
				<Routes>
					<Route path='/' element={<Navigate to='/courses' />} />
					<Route
						path='/courses/*'
						element={
							<ProtectedRoute isAllowed={isAllowed} redirectPath='/login'>
								<CourseContainer />
							</ProtectedRoute>
						}
					/>
					<Route
						path='/registration'
						element={<Registration className='my-auto' />}
					/>
					<Route
						path='/login'
						element={<Login className='my-auto' setUser={setUser} />}
					/>
					<Route path='*' element={<Page404 />} />
				</Routes>
			</main>
		</div>
	);
}

export default App;
