import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Button.scss';

const Button = (props) => {
	const { buttonText, className, fluid, onClick, size, cssStyle, type } = props;
	const btnClass = classNames(
		{ 'btn--fluid': fluid },
		className,
		'btn',
		`btn--style--${cssStyle}`,
		`btn--size--${size}`
	);

	return (
		<button type={type} className={btnClass} onClick={onClick}>
			{buttonText}
		</button>
	);
};

Button.propTypes = {
	buttonText: PropTypes.string.isRequired,
	className: PropTypes.string,
	fluid: PropTypes.bool,
	onClick: PropTypes.func,
	size: PropTypes.string,
	cssStyle: PropTypes.string,
	type: PropTypes.string,
};

Button.defaultProps = {
	className: '',
	fluid: false,
	onClick: () => {},
	size: 'md',
	cssStyle: 'outline-black',
	type: 'button',
};

export default Button;
