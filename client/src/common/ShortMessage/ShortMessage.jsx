import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './ShortMessage.scss';

const ShortMessage = (props) => {
	const { className, message, type } = props;
	const messageClass = classNames(
		className,
		'short-message',
		`short-message--${type}`
	);

	return <span className={messageClass}>{message}</span>;
};

ShortMessage.propTypes = {
	className: PropTypes.string,
	message: PropTypes.string,
	type: PropTypes.oneOf(['info', 'alert']),
};

ShortMessage.defaultProps = {
	className: '',
	type: 'info',
};

export default ShortMessage;
