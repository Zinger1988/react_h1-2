import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './ShortInfoList.scss';

const ShortInfoList = (props) => {
	const { className, listData } = props;
	const shortInfoClass = classNames('short-info', className);

	return (
		<ul className={shortInfoClass}>
			{listData.map((listItem, i) => (
				<li key={i} className='short-info__item'>
					{listItem.title && <b>{listItem.title}:</b>} {listItem.data}
				</li>
			))}
		</ul>
	);
};

ShortInfoList.propTypes = {
	className: PropTypes.string,
	listData: PropTypes.arrayOf(
		PropTypes.shape({
			data: PropTypes.string.isRequired,
			title: PropTypes.string,
		})
	).isRequired,
};

ShortInfoList.defaultProps = {
	className: '',
};

export default ShortInfoList;
