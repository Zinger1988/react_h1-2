import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useField } from 'formik';

import ShortMessage from '../ShortMessage/ShortMessage';

import './Input.scss';

const Input = (props) => {
	const {
		className,
		label,
		name,
		onBlur,
		onChange,
		placeholder,
		cssStyle,
		type,
	} = props;
	const [field, meta] = useField(name);
	const inputClass = classNames(
		className,
		'input',
		`input--style--${cssStyle}`
	);
	const isError = meta.error && meta.touched;

	const handleChange = (e) => {
		field.onChange(e);
		onChange(e);
	};

	return (
		<label className={inputClass}>
			{label && <span className='input__label-title'>{label}</span>}
			<input
				{...field}
				type={type}
				name={name}
				className='input__control'
				placeholder={placeholder}
				onKeyUp={handleChange}
				onBlur={onBlur}
			/>
			{isError && (
				<ShortMessage
					className='input__message'
					type='alert'
					message={meta.error}
				/>
			)}
		</label>
	);
};

Input.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string,
	name: PropTypes.string.isRequired,
	onBlur: PropTypes.func,
	onChange: PropTypes.func,
	placeholder: PropTypes.string,
	cssStyle: PropTypes.string,
	type: PropTypes.string,
	value: PropTypes.string,
};

Input.defaultProps = {
	className: '',
	label: '',
	onBlur: () => {},
	onChange: () => {},
	placeholder: '',
	cssStyle: 'black-outline',
	type: 'text',
	value: '',
};

export default Input;
