import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useField } from 'formik';

import ShortMessage from '../ShortMessage/ShortMessage';

import './Textarea.scss';

const Textarea = (props) => {
	const { className, label, name, onBlur, onChange, placeholder, style } =
		props;
	const [field, meta] = useField(name);
	const textareaClass = classNames(
		className,
		'textarea',
		`textarea--style--${style}`
	);
	const isError = meta.error && meta.touched;

	const handleChange = (e) => {
		onChange(e);
		return field.onChange(e);
	};

	return (
		<label className={textareaClass}>
			{label && <span className='textarea__label-title'>{label}</span>}
			<textarea
				{...field}
				className='textarea__control'
				placeholder={placeholder}
				name={name}
				onChange={handleChange}
				onBlur={onBlur}
			/>
			{isError && (
				<ShortMessage
					className='textarea__message'
					type='alert'
					message={meta.error}
				/>
			)}
		</label>
	);
};

Textarea.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string,
	name: PropTypes.string.isRequired,
	onBlur: PropTypes.func,
	onChange: PropTypes.func,
	placeholder: PropTypes.string,
	style: PropTypes.string,
};

Textarea.defaultProps = {
	className: '',
	label: '',
	onBlur: () => {},
	onChange: () => {},
	placeholder: '',
	style: 'black-outline',
};

export default Textarea;
