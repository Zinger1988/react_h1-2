import React, { useEffect } from 'react';
import axios from 'axios';
import { Form, withFormik } from 'formik';
import { Link, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import ShortMessage from '../../common/ShortMessage/ShortMessage';

import validationSchema from './validationSchema';
import { API } from '../../constants';
import './Login.scss';

const Login = (props) => {
	const { className } = props;
	const navigate = useNavigate();
	const loginClass = classNames('login', className);
	const formStatus = props.status || {
		successful: false,
		result: '',
		errors: [],
	};

	useEffect(() => {
		formStatus.successful && navigate('/courses');
	});

	return (
		<div className={loginClass}>
			<div className='container container--narrow'>
				<h1 className='page-title'>Login</h1>

				<Form className='login__form form'>
					<div className='form__body'>
						<Input
							className='form__input'
							name='email'
							label='Email'
							placeholder='Enter your email...'
						/>
						<Input
							className='form__input'
							name='password'
							label='Password'
							placeholder='Enter your password...'
						/>
					</div>
					<div className='form__footer'>
						<Button
							type='submit'
							buttonText='Login'
							cssStyle='solid-yellow'
							size='lg'
							fluid
						/>
					</div>
				</Form>
				<p>
					If you not have an account you can{' '}
					<Link to='/registration'>register</Link>
				</p>
				{formStatus.result && (
					<ShortMessage
						className='input__message'
						type='alert'
						message={formStatus.result}
					/>
				)}
				{formStatus.errors && formStatus.errors.length > 0 && (
					<ShortMessage
						className='input__message'
						type='alert'
						message={formStatus.errors.join(', ')}
					/>
				)}
			</div>
		</div>
	);
};

const handleSubmit = async (values, { setStatus, props }) => {
	try {
		const { setUser } = props;
		const response = await axios
			.post(
				API.user.login,
				{
					...values,
				},
				{
					headers: {
						'Content-Type': 'application/json',
					},
				}
			)
			.catch((axiosError) => {
				throw axiosError.response.data;
			});

		localStorage.setItem('token', response.data.result);
		localStorage.setItem('user', JSON.stringify(response.data.user));
		setUser(response.data.user);
		setStatus({
			successful: true,
			result: '',
			errors: [],
		});
	} catch (errorData) {
		setStatus(errorData);
	}
};

Login.propTypes = {
	className: PropTypes.string,
};

Login.defaultProps = {
	className: '',
};

export default withFormik({
	mapPropsToValues: (props) => ({
		email: '',
		password: '',
	}),
	handleSubmit,
	validationSchema,
})(Login);
