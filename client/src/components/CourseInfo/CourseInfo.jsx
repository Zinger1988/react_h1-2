import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Button from '../../common/Button/Button';
import ShortInfoList from '../../common/ShortInfoList/ShortInfoList';

import './CourseInfo.scss';
import Courses from '../Courses/Courses';

const CourseInfo = (props) => {
	const { className, coursesData } = props;
	const params = useParams();
	const navigate = useNavigate();
	const currentCourse = coursesData.find((course) => course.id === params.id);
	const courseInfoClass = classNames('course-info', className);

	const shortInfoData = [
		{ title: 'ID', data: currentCourse.id },
		{ title: 'Authors', data: currentCourse.authors },
		{ title: 'Duration', data: currentCourse.duration },
		{ title: 'Created', data: currentCourse.creationDate },
	];

	return (
		<div className={courseInfoClass}>
			<div className='container'>
				<div className='course-info__head'>
					<h1 className='course-info__title page-title'>
						{currentCourse.title}
					</h1>
					<Button
						className='course-info__back-btn'
						buttonText='Back to courses'
						onClick={() => navigate('../')}
					/>
				</div>
				<section className='course-desc course-info__course-desc'>
					<div className='course-desc__col'>
						<p className='course-desc__about'>{currentCourse.description}</p>
					</div>
					<div className='course-desc__col'>
						<ShortInfoList
							className='course-desc__short-info'
							listData={shortInfoData}
						/>
					</div>
				</section>
			</div>
		</div>
	);
};

CourseInfo.propTypes = {
	className: PropTypes.string,
	coursesData: PropTypes.arrayOf(
		PropTypes.exact({
			authors: PropTypes.string,
			creationDate: PropTypes.string,
			description: PropTypes.string,
			duration: PropTypes.string,
			id: PropTypes.string,
			title: PropTypes.string,
		})
	).isRequired,
};

Courses.defaultProps = {
	className: '',
};

export default CourseInfo;
