import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import './Header.scss';

const Header = (props) => {
	const { className, user, setUser } = props;
	const headerClass = classNames('header', className);

	return (
		<header className={headerClass}>
			<div className='container header__container'>
				<Link className='header__logo' to='/'>
					<Logo className='header__logo-img' />
				</Link>

				{user && (
					<>
						<div className='header__user'>{user.email}</div>
						<Button
							onClick={logout}
							className='header__logout-btn'
							buttonText='Logout'
						/>
					</>
				)}
			</div>
		</header>
	);

	function logout() {
		localStorage.removeItem('token');
		localStorage.removeItem('user');
		setUser(null);
	}
};

Header.propTypes = {
	className: PropTypes.string,
	setUser: PropTypes.func.isRequired,
};

Header.defaultProps = {
	className: '',
};

export default Header;
