import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Logo.scss';

const Logo = (props) => {
	const { className } = props;
	const logoClass = classNames('logo', className);
	return <img className={logoClass} src='/logo.svg' alt='logo' />;
};

Logo.propTypes = {
	className: PropTypes.string,
};

Logo.defaultProps = {
	className: '',
};

export default Logo;
