import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { useFormikContext } from 'formik';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import { API } from '../../../../constants';
import Author from '../../../../helpers/Author';

const CreateAuthor = (props) => {
	const { authorsList, setAuthorsList } = props;
	const formikContext = useFormikContext();

	return (
		<>
			<Input
				className='course-form__input'
				placeholder='Enter author name...'
				label='Author name'
				name='newAuthor'
			/>
			<Button
				onClick={() => createAuthor(formikContext)}
				buttonText='Create author'
			/>
		</>
	);

	async function createAuthor(formikProps) {
		const { setFieldValue } = formikProps;
		const { newAuthor } = formikProps.values;

		if (newAuthor.trim()) {
			try {
				const response = await axios
					.post(API.authors.add, new Author(newAuthor.trim()), {
						headers: {
							'Content-Type': 'application/json',
							Authorization: localStorage.getItem('token'),
						},
					})
					.catch((axiosError) => {
						throw axiosError;
					});
				setAuthorsList([...authorsList, response.data.result]);
			} catch (errorData) {
				// setStatus(errorData);
				console.log(errorData);
			} finally {
				setFieldValue('newAuthor', '');
			}
		}
	}
};

CreateAuthor.propTypes = {
	authorsList: PropTypes.arrayOf(
		PropTypes.exact({
			id: PropTypes.string,
			name: PropTypes.string,
		})
	).isRequired,
	setAuthorsList: PropTypes.func.isRequired,
};

export default CreateAuthor;
