import React, { useState } from 'react';
import axios from 'axios';
import { Form, Formik } from 'formik';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as yup from 'yup';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import Textarea from '../../common/Textarea/Textarea';
import AuthorsList from './components/AuthorsList/AuthorsList';
import CourseDuration from './components/CourseDuration/CourseDuration';
import CreateAuthor from './components/CreateAuthor/CreateAuthor';
import ShortMessage from '../../common/ShortMessage/ShortMessage';
import classNames from 'classnames';

import { API } from '../../constants';
import Course from '../../helpers/Course';
import './CreateCourse.scss';

const CreateCourse = (props) => {
	const {
		getAuthorsById,
		authorsList,
		setAuthorsList,
		coursesList,
		setCoursesList,
		className,
	} = props;
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [duration, setDuration] = useState('');
	const navigate = useNavigate();
	const selectedAuthors = getAuthorsById(authorsList, courseAuthors);
	const createCourseClass = classNames('create-course', className);
	const availableAuthors = authorsList.filter((author) => {
		return !courseAuthors.some((auth) => auth === author.id);
	});

	const validationSchema = yup.object().shape({
		title: yup.string().required('This field is required'),
		description: yup.string().required('This field is required'),
		duration: yup
			.number()
			.typeError('Only numbers allowed')
			.required('This field is required'),
		courseAuthors: yup
			.array()
			.test('course-authors', 'You should add at least one author', () => {
				return courseAuthors.length > 0;
			}),
	});

	return (
		<Formik
			initialValues={{
				title: '',
				description: '',
				duration: '',
				newAuthor: '',
			}}
			validateOnChange={false}
			validateOnBlur={false}
			validationSchema={validationSchema}
			onSubmit={handleSubmit}
		>
			{(formikProps) => (
				<div className={createCourseClass}>
					<div className='container create-course__container'>
						<Form className='create-course__form course-form'>
							<fieldset className='course-form__head'>
								<Input
									className='course-form__input course-form__input--title'
									placeholder='Enter title...'
									label='Title'
									name='title'
								/>
								<Button
									cssStyle='solid-yellow'
									buttonText='Create course'
									type='submit'
								/>
								<Button
									onClick={() => navigate('/courses')}
									cssStyle='solid-black'
									buttonText='Cancel'
								/>
								<Textarea
									className='course-form__textarea course-form__textarea--description'
									placeholder='Enter description...'
									label='Description'
									name='description'
								/>
							</fieldset>
							<fieldset className='course-form__body'>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Create author</h3>
									<CreateAuthor
										authorsList={authorsList}
										setAuthorsList={setAuthorsList}
									/>
								</div>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Authors</h3>
									<AuthorsList
										className='course-form__author-list'
										list={availableAuthors}
										onClick={handleAvailableAuthors}
										buttonText='Add author'
									/>
								</div>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Duration</h3>
									<CourseDuration
										setDuration={setDuration}
										duration={duration}
									/>
								</div>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Course authors</h3>
									{formikProps.errors.courseAuthors && (
										<ShortMessage
											className='course-form__message'
											type='alert'
											message={formikProps.errors.courseAuthors}
										/>
									)}
									<AuthorsList
										className='course-form__author-list'
										list={selectedAuthors}
										onClick={handleSelectedAuthors}
										buttonText='Remove author'
									/>
								</div>
							</fieldset>
						</Form>
					</div>
				</div>
			)}
		</Formik>
	);

	async function handleSubmit(values, { setStatus, props }) {
		const courseData = new Course({ ...values, authors: courseAuthors });

		try {
			const response = await axios
				.post(API.courses.add, courseData, {
					headers: {
						'Content-Type': 'application/json',
						Authorization: localStorage.getItem('token'),
					},
				})
				.catch((axiosError) => {
					throw axiosError;
				});
			setCoursesList([...coursesList, response.data.result]);
			navigate('/courses');
		} catch (errorData) {
			//TODO handle error
			console.log(errorData);
		}
	}

	function handleSelectedAuthors(author) {
		setCourseAuthors(courseAuthors.filter((a) => a !== author.id));
	}

	function handleAvailableAuthors(author) {
		setCourseAuthors([...courseAuthors, author.id]);
	}
};

CreateCourse.propTypes = {
	getAuthorsById: PropTypes.func.isRequired,
	authorsList: PropTypes.arrayOf(
		PropTypes.exact({
			id: PropTypes.string,
			name: PropTypes.string,
		})
	).isRequired,
	setAuthorsList: PropTypes.func.isRequired,
	coursesList: PropTypes.arrayOf(
		PropTypes.exact({
			title: PropTypes.string,
			description: PropTypes.string,
			creationDate: PropTypes.string,
			duration: PropTypes.number,
			authors: PropTypes.arrayOf(PropTypes.string.isRequired),
			id: PropTypes.string,
		})
	).isRequired,
	setCoursesList: PropTypes.func.isRequired,
	className: PropTypes.string,
};

export default CreateCourse;
