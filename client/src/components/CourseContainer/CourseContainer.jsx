import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Routes, Route } from 'react-router-dom';

import Courses from '../Courses/Courses';
import CreateCourse from '../CreateCourse/CreateCourse';
import CourseInfo from '../CourseInfo/CourseInfo';

import pipeDuration from '../../helpers/pipeDuration';
import getAuthorsById from '../../helpers/getAuthorsById';
import { API } from '../../constants';

const CourseContainer = () => {
	const [coursesList, setCoursesList] = useState([]);
	const [authorsList, setAuthorsList] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const coursesData = coursesList.map((course) => {
		const courseAuthors = getAuthorsById(authorsList, course.authors);
		const courseAuthorNames = courseAuthors.map((author) => author.name);
		return {
			...course,
			duration: pipeDuration(course.duration),
			authors: courseAuthorNames.join(', '),
		};
	});

	useEffect(() => {
		const coursesReq = axios(API.courses.all);
		const authorsReq = axios(API.authors.all);
		Promise.all([coursesReq, authorsReq]).then(([coursesRes, authorsRes]) => {
			setCoursesList(coursesRes.data.result);
			setAuthorsList(authorsRes.data.result);
			setIsLoading(false);
		});
	}, []);

	if (isLoading) {
		return <div>Loading</div>;
	}

	return (
		<Routes>
			<Route
				index
				element={<Courses className='app__courses' coursesData={coursesData} />}
			/>
			<Route
				path='add'
				element={
					<CreateCourse
						getAuthorsById={getAuthorsById}
						coursesList={coursesList}
						setCoursesList={setCoursesList}
						authorsList={authorsList}
						setAuthorsList={setAuthorsList}
					/>
				}
			/>
			<Route path=':id' element={<CourseInfo coursesData={coursesData} />} />
		</Routes>
	);
};

export default CourseContainer;
