import * as yup from 'yup';

const validationSchema = yup.object().shape({
	name: yup.string().required(),
	email: yup
		.string()
		.matches(/.+@[^@]+\.[^@]{2,}$/, 'Invalid email')
		.required(),
	password: yup.string().min(8).required(),
});

export default validationSchema;
