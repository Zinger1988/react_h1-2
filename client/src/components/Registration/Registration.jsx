import React, { useEffect } from 'react';
import { Form, withFormik } from 'formik';
import { Link, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from 'axios';
import classNames from 'classnames';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import ShortMessage from '../../common/ShortMessage/ShortMessage';

import validationSchema from './validationSchema';
import { API } from '../../constants';
import './Registration.scss';

const Registration = (props) => {
	const { className } = props;
	const navigate = useNavigate();
	const registrationClass = classNames('registration', className);
	const formStatus = props.status || {
		successful: false,
		result: '',
		errors: [],
	};

	useEffect(() => {
		formStatus.successful && navigate('/courses');
	});

	return (
		<div className={registrationClass}>
			<div className='container container--narrow'>
				<h1 className='page-title'>Registration</h1>

				<Form className='registration__form form'>
					<div className='form__body'>
						<Input
							className='form__input'
							name='name'
							label='Name'
							placeholder='Enter your name...'
						/>
						<Input
							className='form__input'
							name='email'
							label='Email'
							placeholder='Enter your email...'
						/>
						<Input
							className='form__input'
							name='password'
							label='Password'
							placeholder='Enter your password...'
						/>
					</div>
					<div className='form__footer'>
						<Button
							type='submit'
							buttonText='Registration'
							cssStyle='solid-yellow'
							size='lg'
							fluid
						/>
					</div>
				</Form>
				<p>
					If you have an account you can <Link to='/login'>login</Link>
				</p>
				{formStatus.result && (
					<ShortMessage type='alert' message={formStatus.result} />
				)}
				{formStatus.errors && formStatus.errors.length > 0 && (
					<ShortMessage type='alert' message={formStatus.errors.join(', ')} />
				)}
			</div>
		</div>
	);
};

const handleSubmit = async (values, { setStatus, props }) => {
	try {
		await axios
			.post(
				API.user.registration,
				{
					...values,
				},
				{
					headers: {
						'Content-Type': 'application/json',
					},
				}
			)
			.catch((axiosError) => {
				throw axiosError.response.data;
			});

		setStatus({
			successful: true,
			result: '',
			errors: [],
		});
	} catch (errorData) {
		setStatus(errorData);
	}
};

Registration.propTypes = {
	className: PropTypes.string,
};

Registration.defaultProps = {
	className: '',
};

export default withFormik({
	mapPropsToValues: (props) => ({
		name: '',
		email: '',
		password: '',
	}),
	handleSubmit,
	validationSchema,
})(Registration);
