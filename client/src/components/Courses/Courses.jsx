import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import classNames from 'classnames';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';

import './Courses.scss';

const Courses = (props) => {
	const { className, coursesData } = props;
	const [foundCourses, setFoundCourses] = useState(coursesData);
	const navigate = useNavigate();
	const coursesClass = classNames('courses', className);
	const courseCards = foundCourses.map((card) => (
		<CourseCard key={card.id} {...card} />
	));

	return (
		<div className={coursesClass}>
			<div className='container'>
				<div className='courses__head'>
					<SearchBar
						className='courses__search'
						resetSearchCards={resetSearchCards}
						searchCards={searchCards}
					/>
					<Button
						className='courses__create-btn'
						cssStyle='outline-white'
						onClick={() => navigate('add')}
						buttonText='Add new course'
					/>
				</div>
				<div className='courses__list'>
					{courseCards.length > 0 && courseCards}
					{!courseCards.length && <b>No courses by your request</b>}
				</div>
			</div>
		</div>
	);

	function searchCards(searchStr) {
		const searchRes = coursesData.filter((card) => {
			const regexp = new RegExp(searchStr, 'gi');
			return card.title.match(regexp) || card.id.match(regexp);
		});
		setFoundCourses(searchRes);
	}

	function resetSearchCards() {
		setFoundCourses(coursesData);
	}
};

Courses.propTypes = {
	className: PropTypes.string,
	coursesData: PropTypes.arrayOf(
		PropTypes.exact({
			authors: PropTypes.string,
			creationDate: PropTypes.string,
			description: PropTypes.string,
			duration: PropTypes.string,
			id: PropTypes.string,
			title: PropTypes.string,
		})
	).isRequired,
};

Courses.defaultProps = {
	className: '',
};

export default Courses;
