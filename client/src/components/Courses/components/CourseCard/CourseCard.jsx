import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import classNames from 'classnames';

import Button from '../../../../common/Button/Button';
import ShortInfoList from '../../../../common/ShortInfoList/ShortInfoList';

import './CourseCard.scss';

const CourseCard = (props) => {
	const { id, title, duration, creationDate, description, authors, className } =
		props;
	const navigate = useNavigate();
	const courseCardClass = classNames('course-card', className);

	const shortInfoData = [
		{ title: 'Authors', data: authors },
		{ title: 'Duration', data: duration },
		{ title: 'Created', data: creationDate },
	];

	return (
		<article className={courseCardClass}>
			<div className='course-card__col'>
				<h2 className='course-card__title'>{title}</h2>
				<p className='course-card__id'>ID: {id}</p>
				<p className='course-card__description'>{description}</p>
			</div>
			<div className='course-card__col'>
				<ShortInfoList className='course-card__info' listData={shortInfoData} />
				<Button buttonText='Show course' onClick={() => navigate(id)} />
			</div>
		</article>
	);
};

CourseCard.propTypes = {
	authors: PropTypes.string.isRequired,
	creationDate: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
	duration: PropTypes.string.isRequired,
	id: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
};

export default CourseCard;
