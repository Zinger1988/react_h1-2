import React from 'react';
import PropTypes from 'prop-types';
import { Form, withFormik } from 'formik';
import classNames from 'classnames';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import './SearchBar.scss';

const SearchBar = (props) => {
	const {
		className,
		resetSearchCards,
		setFieldValue,
		values: { search },
	} = props;

	const searchBarClass = classNames('search-bar', className);
	const handleChange = () => !search && resetSearchCards();
	const handleBlur = () => !search.trim() && setFieldValue('search', '');

	return (
		<Form className={searchBarClass}>
			<Input
				className='search-bar__input'
				placeholder='Enter course name or course ID...'
				cssStyle='white-solid'
				name='search'
				onChange={handleChange}
				onBlur={handleBlur}
			/>
			<Button
				type='submit'
				buttonText='Search'
				className='search-bar__search-btn'
				cssStyle='solid-yellow'
			/>
		</Form>
	);
};

SearchBar.propTypes = {
	className: PropTypes.string,
	searchCards: PropTypes.func.isRequired,
	resetSearchCards: PropTypes.func.isRequired,
};

SearchBar.defaultProps = {
	className: '',
};

const handleSubmit = (values, helpers) => {
	const { search } = values;
	const { searchCards } = helpers.props;
	searchCards(search.trim());
};

export default withFormik({
	mapPropsToValues: (props) => ({
		search: '',
	}),
	handleSubmit,
})(SearchBar);
